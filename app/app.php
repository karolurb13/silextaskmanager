<?php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__.'/bootstrap.php';

$app->get('/', function() use($app) {
    $getTasks = $app['db']->fetchAll('SELECT * FROM tasks');
    
    return $app['twig']->render('index.html.twig',
            [
                'tasks' => $getTasks,
            ]
        );
})
->bind('index');

$app->get('/dodaj', function() use($app) {
    
    $dodaj = $app['form.factory']->createBuilder('form')
        ->add('_task')
        ->getForm();
    
    return $app['twig']->render('dodaj.html.twig',
            [
                'form' => $dodaj->createView(),
            ]
        );
})
->bind('dodaj');

$app->get('/edytuj/{id}', function(Request $request, $id) use($app) {
    
    $dodaj = $app['form.factory']->createBuilder('form')
        ->add('_task')
        ->getForm();
    
    return $app['twig']->render('edytuj.html.twig',
            [
                'id'   => $id,
                'form' => $dodaj->createView(),
            ]
        );
})
->bind('edytuj/{id}');

$app->post('/dodaj_do_bazy', function(Request $request) use($app) {
    
    $dodaj = $app['form.factory']->createBuilder('form')
        ->add('_task')
        ->getForm();

    $dodaj->handleRequest($request);

    if ($dodaj->isValid()) {
        $data = $dodaj->getData();
        
        var_dump($app['dbs']);
        
        $sql = "INSERT INTO tasks VALUES ( '', ?)";
        $app['db']->executeUpdate($sql, [$data['_task']]);
    }
    
    return $app->redirect('/');
    
})
->bind('dodaj_do_bazy');

$app->post('/edytuj_z_bazy/{id}', function(Request $request, $id) use($app) {
    
    $dodaj = $app['form.factory']->createBuilder('form')
        ->add('_task')
        ->getForm();

    $dodaj->handleRequest($request);

    if ($dodaj->isValid()) {
        $data = $dodaj->getData();
        
        var_dump($app['dbs']);
        
        $sql = "UPDATE tasks SET text = ? WHERE id = ?";
        $app['db']->executeUpdate($sql, [$data['_task'], $id]);
    }
    
    return $app->redirect('/');
    
})
->bind('edytuj_z_bazy/{id}');

$app->get('/usun_z_bazy/{id}', function(Request $request, $id) use($app) {
    
    $sql = "DELETE FROM tasks WHERE id = ?";
    $app['db']->executeUpdate($sql, [$id]);
    
    return $app->redirect('/');
    
})
->bind('usun_z_bazy/{id}');


return $app;